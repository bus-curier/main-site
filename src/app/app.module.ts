import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {PageHeaderModule} from './modules/page-header/page-header.module';
import {PageFooterModule} from './modules/page-footer/page-footer.module';
import {EshopsPageModule} from './pages/eshops-page/eshops-page.module';
import {DocumentsPageModule} from './pages/documents-page/documents-page.module';
import {WorkInTeamPageModule} from './pages/work-in-team-page/work-in-team-page.module';
import {IndexPageModule} from './pages/index-page/index-page.module';
import {FeedbackPageModule} from './pages/feedback-page/feedback-page.module';
import {AboutPageModule} from './pages/about-page/about-page.module';
import {ContactsPageModule} from './pages/contacts-page/contacts-page.module';

import { AngularFireModule} from '@angular/fire';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import {AngularFirestore, AngularFirestoreModule} from '@angular/fire/firestore';
import {environment} from '../environments/environment';
import {ModalsModule} from './modals/modals.module';

import {registerLocaleData} from '@angular/common';
import ruLocale from '@angular/common/locales/ru';
import {NotFoundPageModule} from './pages/not-found-page/not-found-page.module';

registerLocaleData(ruLocale, 'ru');

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    PageHeaderModule,
    PageFooterModule,
    IndexPageModule,
    EshopsPageModule,
    DocumentsPageModule,
    WorkInTeamPageModule,
    FeedbackPageModule,
    AboutPageModule,
    ContactsPageModule,
    NotFoundPageModule,
    // IndexPageModule,
    // ModalsModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFireDatabaseModule,
    AngularFirestoreModule
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
