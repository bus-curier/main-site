import {Component, OnInit} from '@angular/core';
import {SimpleModalComponent} from 'ngx-simple-modal';

@Component({
  selector: 'app-cse-modal',
  templateUrl: './cse-modal.component.html',
  styleUrls: ['./cse-modal.component.scss']
})
export class CseModalComponent extends SimpleModalComponent<null, null> implements OnInit {
  public loaded = false;

  constructor() {
    super();
  }

  ngOnInit(): void {}

  onLoad() {
    this.loaded = true;
  }
}
