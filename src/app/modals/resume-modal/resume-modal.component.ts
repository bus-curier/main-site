import {Component, OnDestroy, OnInit} from '@angular/core';
import {SimpleModalComponent, SimpleModalService} from 'ngx-simple-modal';
import formFieldMeta from '../../core/form/formFieldMeta';
import fieldError from '../../core/form/fieldError';
import FormControlName from 'src/app/core/maps/FormControlName';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Pattern} from '../../core/pattern/pattern';
import {environment} from '../../../environments/environment';
import {delay, take, tap} from 'rxjs/operators';
import {CommonService} from '../../core/services/common/common.service';
import {FormUtilsService} from '../../core/services/form-utils.service';
import {AlertModalComponent} from '../alert-modal/alert-modal.component';
import {ConfirmModalComponent} from '../confirm-modal/confirm-modal.component';
import {Router} from '@angular/router';

@Component({
  selector: 'app-resume-modal',
  templateUrl: './resume-modal.component.html',
  styleUrls: ['./resume-modal.component.scss']
})
export class ResumeModalComponent extends SimpleModalComponent<null, null> implements OnInit, OnDestroy {
  public FormFieldMeta = formFieldMeta;
  public FormControlName = FormControlName;
  public form: FormGroup;
  public isLoading = false;
  public errorMessage = '';

  constructor(
    private commonService: CommonService,
    public formUtils: FormUtilsService,
    private router: Router,
    private simpleModal: SimpleModalService) {
    super();
  }

  ngOnInit(): void {
    this.form = new FormGroup({
      [FormControlName.Fio]: new FormControl('',
        [Validators.required, Validators.pattern(Pattern.Text),
          Validators.minLength(2)]),
      [FormControlName.Email]: new FormControl('',
        [Validators.required, Validators.email]),
      [FormControlName.Tel]: new FormControl('',
        [Validators.required, Validators.pattern(Pattern.Phone)]),
      [FormControlName.About]: new FormControl('',
        [Validators.required,  Validators.pattern(Pattern.TextWithNumbersAndSymbols),
          Validators.minLength(2)]),
      [FormControlName.Agree]: new FormControl(false, [Validators.required]),
      [FormControlName.Captcha]: new FormControl('', [Validators.required])
    });
  }

  onSubmit() {
    this.form.markAllAsTouched();

    if (this.form.invalid) {
      return;
    }

    this.isLoading = true;

    const data = this.form.value;

    this.commonService.sendMail({
      'api-key': environment.api_key,
      sender: data.fio,
      phone: data.tel,
      email: data.email,
      message: data.message,
      emailto: 'personal@untd.pro'
    })
      .pipe(
        take(1))
      .subscribe(() => {
          this.isLoading = false;
          this.close();

          this.alert('Ваша заявка отправлена!')
            .pipe(take(1))
            .subscribe(() => {
              this.form.reset();
            });
        },
        (error) => {
          this.isLoading = false;
          this.errorMessage = 'Заявка не была отправлена. Попробуйте еще раз.';
        });
  }

  captchaResolved(value: string) {

    if (!value) {
      this.form.get(FormControlName.Agree).setValue(false);
    }
  }

  executeCaptcha(captcha) {
    captcha.execute();
    this.form.markAllAsTouched();
  }

  alert(message, btnText = null) {
    return this.simpleModal.addModal(AlertModalComponent, {
      message,
      btnText
    });
  }

  confirm(e) {
    e.preventDefault();

    this.simpleModal.addModal(ConfirmModalComponent, {
      message: 'Перейти на страницу "Политика конфиденциальности"?'
    }).subscribe((isConfirmed) => {
      if (isConfirmed) {
        this.router.navigate([]).then((result) => {
          window.open('/info/privacy-policy', '_blank');
        });
      }
    });
  }

  ngOnDestroy(): void {
  }

}
