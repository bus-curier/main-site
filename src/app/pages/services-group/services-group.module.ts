import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ServicesGroupComponent } from './services-group.component';
import {ServicesGroupRoutingModule} from './services-group-routing.module';
import {SharedModule} from '../../shared/shared.module';
import {KcePageModule} from './kce-page/kce-page.module';
import {ServicesPageModule} from './services-page/services-page.module';
import {CargoInsurancePageModule} from './cargo-insurance-page/cargo-insurance-page.module';
import {CourierPageModule} from './courier-page/courier-page.module';
import {NonStandardTasksPageModule} from './non-standard-tasks-page/non-standard-tasks-page.module';
import {IndexPageModule} from './index-page/index-page.module';

@NgModule({
  declarations: [
    ServicesGroupComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    ServicesPageModule,
    IndexPageModule,
    CargoInsurancePageModule,
    CourierPageModule,
    // NonStandardTasksPageModule,
    KcePageModule,
    ServicesGroupRoutingModule,
  ]
})
export class ServicesGroupModule { }
