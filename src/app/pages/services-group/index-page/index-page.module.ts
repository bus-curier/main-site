import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {IndexPageComponent} from './index-page.component';
import {SharedModule} from '../../../shared/shared.module';
import { TrafficComponent } from './components/traffic/traffic.component';
import { ServicesComponent } from './components/services/services.component';



@NgModule({
  declarations: [
    IndexPageComponent,
    TrafficComponent,
    ServicesComponent
  ],
  imports: [
    CommonModule,
    SharedModule
  ]
})
export class IndexPageModule { }
