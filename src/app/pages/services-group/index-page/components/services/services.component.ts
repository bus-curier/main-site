import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-services',
  templateUrl: './services.component.html',
  styleUrls: ['./services.component.scss']
})
export class ServicesComponent implements OnInit {
  public imagesPath = '/assets/images/pages/services-group/index-page/components/services/images';

  constructor() { }

  ngOnInit(): void {
  }

}
