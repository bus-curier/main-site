import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-traffic',
  templateUrl: './traffic.component.html',
  styleUrls: ['./traffic.component.scss']
})
export class TrafficComponent implements OnInit {
  public imagesPath = '/assets/images/pages/services-group/index-page/components/traffic/images';

  constructor() { }

  ngOnInit(): void {
  }

}
