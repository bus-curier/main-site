import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ServicesGroupComponent} from './services-group.component';
import {CargoInsurancePageComponent} from './cargo-insurance-page/cargo-insurance-page.component';
import {CourierPageComponent} from './courier-page/courier-page.component';
import {KcePageComponent} from './kce-page/kce-page.component';
import {ServicesPageComponent} from './services-page/services-page.component';
import {NonStandardTasksPageComponent} from './non-standard-tasks-page/non-standard-tasks-page.component';
import {NonStandardTasksPageModule} from './non-standard-tasks-page/non-standard-tasks-page.module';
import {IndexPageComponent} from './index-page/index-page.component';


const routes: Routes = [
  {path: '', component: ServicesGroupComponent, data: { title: 'Наши услуги' }, children: [
      { path: '', redirectTo: '/services/index', pathMatch: 'full'},
      { path: 'index', component:  IndexPageComponent, data: { title: 'Все услуги' }},
      { path: 'our-services', component:  ServicesPageComponent},
      { path: 'cargo-insurance', component:  CargoInsurancePageComponent, data: { title: 'Страхование груза' }},
      { path: 'courier', component:  CourierPageComponent, data: { title: 'Забор и доставка курьером' }},
      { path: 'kce', component:  KcePageComponent, data: { title: 'Грузоперевозки по России' }},
      { path: 'non-standard-tasks', loadChildren: () => import('./non-standard-tasks-page/non-standard-tasks-page.module').then((m) => m.NonStandardTasksPageModule)},
    ]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ServicesGroupRoutingModule { }
