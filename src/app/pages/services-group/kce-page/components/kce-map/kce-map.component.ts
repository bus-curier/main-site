import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-kce-map',
  templateUrl: './kce-map.component.html',
  styleUrls: ['./kce-map.component.scss']
})
export class KceMapComponent implements OnInit {
  public imagesPath = '/assets/images/pages/services-group/kce-page/components/kce-map/images';

  constructor() { }

  ngOnInit(): void {
  }

}
