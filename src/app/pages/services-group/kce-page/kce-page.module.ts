import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CalcBoxComponent } from './components/calc-box/calc-box.component';
import {SharedModule} from '../../../shared/shared.module';
import {KcePageComponent} from './kce-page.component';
import { ServicesComponent } from './components/services/services.component';
import { KceMapComponent } from './components/kce-map/kce-map.component';

@NgModule({
  declarations: [
    KcePageComponent,
    CalcBoxComponent,
    ServicesComponent,
    KceMapComponent
  ],
  imports: [
    CommonModule,
    SharedModule
  ]
})
export class KcePageModule { }
