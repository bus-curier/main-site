import { Injectable } from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {Office} from '../../../../core/interfaces/calculator';
import {FormService} from '../../../../core/services/form/form.service';

@Injectable({
  providedIn: 'root'
})
export class ContactsService {
  public offices$ = new BehaviorSubject<Array<any>>([]);
  public currentOfficeId$ = new BehaviorSubject('');
  public currentOffice$ = new BehaviorSubject(null);

  constructor(private formService: FormService) {}

  getOffices(): Observable<Office> {
    return this.formService.getOffices();
  }
}
