import { Component, OnInit } from '@angular/core';

const links = [
  {text: 'Как отправить посылку', href: '/info/how-to-send'},
  {text: 'Как получить посылку', href: '/info/how-to-get'},
  {text: 'Правила приемки и отправки грузов', href: '/info/rules-of-send'},
  {text: 'Тарифы на перевозку', href: '/info/rates'},
  {text: 'Упаковки грузов и виды упаковки', href: '/info/packing'},
  {text: 'Хранение груза', href: '/info/storage'},
  {text: 'Доставка грузов и багажа из Аэропорта', href: '/info/from-airport'}
];

@Component({
  selector: 'app-useful-info-page',
  templateUrl: './index-page.component.html',
  styleUrls: ['./index-page.component.scss']
})
export class IndexPageComponent implements OnInit {

  public links = links;

  constructor() { }

  ngOnInit(): void {
  }

}
