import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {InfoGroupComponent} from './info-group.component';
import {HowToSendPageComponent} from './how-to-send-page/how-to-send-page.component';
import {PackingPageComponent} from './packing-page/packing-page.component';
import {RulesOfSendPageComponent} from './rules-of-send-page/rules-of-send-page.component';
import {RatesPageComponent} from './rates-page/rates-page.component';
import {HowToGetPageComponent} from './how-to-get-page/how-to-get-page.component';
import {StoragePageComponent} from './storage-page/storage-page.component';
import {PrivacyPolicyPageComponent} from './privacy-policy-page/privacy-policy-page.component';
import {IndexPageComponent} from './index-page/index-page.component';
import {FromAirportPageComponent} from './from-airport-page/from-airport-page.component';

const routes: Routes = [
  {path: '', component: InfoGroupComponent, data: { title: 'Информация' }, children: [
    { path: '', redirectTo: '/info/index', pathMatch: 'full'},
    { path: 'index', component:  IndexPageComponent, data: { title: 'Полезная информация' }},
    { path: 'how-to-send', component:  HowToSendPageComponent, data: { title: 'Как отправить посылку' }},
    { path: 'how-to-get', component:  HowToGetPageComponent, data: { title: 'Как получить посылку' }},
    { path: 'from-airport', component: FromAirportPageComponent, data: { title: 'Доставка грузов и багажа из Аэропорта' }},
    { path: 'packing', component:  PackingPageComponent, data: { title: 'Упаковки грузов и виды упаковки' }},
    { path: 'storage', component: StoragePageComponent, data: { title: 'Хранение груза на складах Баскурьер' }},
    { path: 'rules-of-send', component:  RulesOfSendPageComponent, data: { title: 'Правила приемки и отправки грузов' }},
    { path: 'rates', component: RatesPageComponent, data: { title: 'Тарифы на перевозку' }},
    { path: 'privacy-policy', component:  PrivacyPolicyPageComponent, data: { title: 'Политика конфиденциальности' }}
  ]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class InfoGroupRoutingModule { }
