import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InfoGroupComponent } from './info-group.component';
import {InfoGroupRoutingModule} from './info-group-routing.module';
import {SharedModule} from '../../shared/shared.module';
import {FromAirportPageModule} from './from-airport-page/from-airport-page.module';
import {HowToSendPageModule} from './how-to-send-page/how-to-send-page.module';
import {PackingPageModule} from './packing-page/packing-page.module';
import {RulesOfSendPageModule} from './rules-of-send-page/rules-of-send-page.module';
import {RatesPageModule} from './rates-page/rates-page.module';
import { HowToGetPageComponent } from './how-to-get-page/how-to-get-page.component';
import { StoragePageComponent } from './storage-page/storage-page.component';
import { PrivacyPolicyPageComponent } from './privacy-policy-page/privacy-policy-page.component';
import {IndexPageModule} from './index-page/index-page.module';

@NgModule({
  declarations: [
    InfoGroupComponent,
    HowToGetPageComponent,
    StoragePageComponent,
    PrivacyPolicyPageComponent
  ],
  imports: [
    IndexPageModule,
    CommonModule,
    SharedModule,
    FromAirportPageModule,
    HowToSendPageModule,
    PackingPageModule,
    RulesOfSendPageModule,
    RatesPageModule,
    InfoGroupRoutingModule
  ]
})
export class InfoGroupModule { }
