import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-packing-page',
  templateUrl: './packing-page.component.html',
  styleUrls: ['./packing-page.component.scss']
})
export class PackingPageComponent implements OnInit {
  public imagesPath = '/assets/images/pages/info-group/package/images';

  constructor() { }

  ngOnInit(): void {
  }

}
