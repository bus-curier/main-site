import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {PackingPageComponent} from './packing-page.component';
import { PackageBoxComponent } from './components/package-box/package-box.component';
import {SharedModule} from '../../../shared/shared.module';


@NgModule({
  declarations: [
    PackingPageComponent,
    PackageBoxComponent
  ],
  imports: [
    CommonModule,
    SharedModule
  ]
})
export class PackingPageModule { }
