import {Component, OnDestroy, OnInit} from '@angular/core';
import {CommonService} from '../../../core/services/common/common.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-privacy-policy-page',
  templateUrl: './privacy-policy-page.component.html',
  styleUrls: ['./privacy-policy-page.component.scss']
})
export class PrivacyPolicyPageComponent implements OnInit, OnDestroy {
  public data;
  private sub: Subscription;

  constructor(private commonService: CommonService) { }

  ngOnInit(): void {
    this.sub = this.commonService.getPrivacyPolicy()
      .subscribe((data) => {
        this.data = data;
    });
  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }
}
