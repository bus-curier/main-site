import {Component, EventEmitter, OnDestroy, OnInit, Output} from '@angular/core';
import Region from '../../../../../core/maps/Region';
import City from '../../../../../core/maps/City';
import fadeIn from '../../../../../core/animations/fadeIn';
import {CommonService} from '../../../../../core/services/common/common.service';
import {map} from 'rxjs/operators';
import {CalculatorService} from '../../../../../core/services/calculator/calculator.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-regions-box',
  templateUrl: './regions-box.component.html',
  styleUrls: ['./regions-box.component.scss']
})
export class RegionsBoxComponent implements OnInit, OnDestroy {
  @Output() changeCity: EventEmitter<any> = new EventEmitter<any>();

  public Region = Region;

  public currentRegion;
  public currentCity;

  public sub: Subscription;

  public regions = [
    {id: 1, name: 'Приморский край'},
    {id: 14, name: 'Хабаровский край'}
  ];

  public cities = {};
  public isLoading = true;

  constructor(private calcService: CalculatorService) { }

  ngOnInit(): void {
    this.setCurrentRegion(1);

    this.sub = this.calcService.getCitiesFrom()
      .subscribe((cities: any) => {
        this.cities[Region.Primorye] = cities.filter((city) => city.site_id !== '14');
        this.cities[Region.Khabarovsk] = cities.filter((city) => city.site_id === '14');
        this.isLoading = false;
      });
  }

  setCurrentRegion(id) {
    this.currentRegion = id;
    this.setCurrentCity(id);
  }

  setCurrentCity(id) {
    this.currentCity = id;
    this.changeCity.emit(id);
  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }
}
