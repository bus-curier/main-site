import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {SharedModule} from '../../../shared/shared.module';
import {RatesPageComponent} from './rates-page.component';
import { RegionsBoxComponent } from './components/regions-box/regions-box.component';
import { CargoInfoComponent } from './components/cargo-info/cargo-info.component';

@NgModule({
  declarations: [
    RatesPageComponent,
    RegionsBoxComponent,
    CargoInfoComponent
  ],
  imports: [
    CommonModule,
    SharedModule
  ]
})
export class RatesPageModule { }
