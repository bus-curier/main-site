import { Component, OnInit } from '@angular/core';
import City from 'src/app/core/maps/City';
import fadeIn from '../../../core/animations/fadeIn';
import {CommonService} from '../../../core/services/common/common.service';
import {debounceTime, map, take, tap} from 'rxjs/operators';
import {BehaviorSubject, zip} from 'rxjs';

@Component({
  selector: 'app-transportation-rates-page',
  templateUrl: './rates-page.component.html',
  styleUrls: ['./rates-page.component.scss'],
  animations: [fadeIn]
})

export class RatesPageComponent implements OnInit {
  public City = City;

  public currentCity;
  public zones;

  public zoneTariffsData$ = new BehaviorSubject([]);

  public docsAndParcelsTariffs;
  public autoPartsTariffs;
  public otherTariffs;

  private ZoneId = {
    "Зона 1": 1,
    "Зона 2": 2,
    "Зона 3": 3,
    "Зона 4": 4,
    "Зона 5": 5,
    "Зона 6": 6
  }

  private ParcelWeight = {
    "0 - 20": 0,
    "20 - 40": 1,
    "40 - 60": 2,
    "60 - 80": 3,
    "80 - 100": 4,
  }

  public isLoading = true;

  constructor(private commonService: CommonService) { }

  ngOnInit(): void {
    zip(
      this.renderDocsAndParcelsTariffs(),
      this.renderZoneTariffs('Автозапчасти'),
      this.renderZoneTariffs('Другое')
      )
      .pipe(
        tap(() => {
          this.isLoading = true;
        })
      )
      .subscribe((data) => {
        if (!(data || data.length)) {
          return;
        }

        this.isLoading = false;

        this.docsAndParcelsTariffs = data[0];
        this.autoPartsTariffs = data[1];
        this.otherTariffs = data[2]
      })
  }

  setCurrentCity(id: any) {
    this.currentCity = id;

    this.commonService.getZoneTariffs(id)
      .pipe(take(1))
      .subscribe((data: any) => {
        this.zoneTariffsData$.next(data);
      });

    this.commonService.getZones(id)
      .pipe(take(1))
      .subscribe((data) => {
         this.zones = data;
      })
  }

  renderZoneTariffs(type) {
    return this.zoneTariffsData$
      .pipe(
        map((zones: any) => {
          return zones.filter((zone) => {
            return zone.main_type === type
          })
        }),
        map((zones: any) => {
          return zones.map((zone) => {
            return { name: zone.type, data: { id: this.ZoneId[zone.zone], price: zone.price, name: zone.zone } };
          });
        }),
        map((zones: any) => {
          const reduced = zones.reduce((acc, {name, data}) => {
            acc[name] ??= {name, data: [null, null, null, null, null, null, null]};
            acc[name].data.splice(data.id - 1, 1, data);
            return acc;
          }, {});

          return Object.values(reduced);
        })
      )
  }

  renderDocsAndParcelsTariffs() {
    return this.zoneTariffsData$
      .pipe(
        map((zones: any) => {
          return zones.filter((zone) => {
            return zone.main_type === 'Документы (формат А4)' || zone.main_type === 'Посылки'
          })
        }),
        map((zones: any) => {
          return zones.map((zone) => {
            return { name: zone.zone, data: { id: this.ZoneId[zone.id], price: zone.price, size: zone.size, weight: zone.weight, type: zone.type } };
          });
        }),
        map((zones: any) => {
          const reduced = zones.reduce((acc, {name, data}) => {
            acc[name] ??= {name: name, data: []};
            acc[name].data.push(data);
            return acc;
          }, {});

          return Object.values(reduced);
        }),
        map((zones: any) => {
          return zones.map(({name, data}) => {
            const reduced = data.reduce((acc, {type, size, weight, price}) => {
              acc[size] ??= {size: size, data: [null, null, null, null, null]};
              // acc[size].data.push({type, size, weight, price});
              acc[size].data.splice(this.ParcelWeight[weight], 1, {type, size, weight, price});
              return acc;
            }, {});

            return {name, data: Object.values(reduced)};
          })
        }),
        map((zones: any) => {
          return zones.map(({name, data}) => {
            const lastItem = data.pop();

            if (lastItem.size) {
              data.push(lastItem);
            } else {
              data[0].docs = lastItem.data[0].price;
            }

            return {name, data};
          });
        }),
      )
  }
}
