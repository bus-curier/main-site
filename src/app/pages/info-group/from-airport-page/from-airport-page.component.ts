import { Component, OnInit } from '@angular/core';
import { LocalStorageService } from 'src/app/core/services/local-storage.service';

@Component({
  selector: 'app-from-airport-page',
  templateUrl: './from-airport-page.component.html',
  styleUrls: ['./from-airport-page.component.scss']
})
export class FromAirportPageComponent implements OnInit {
  public phoneNumber = '';
  public icons = [
    {
      icon: `aeroflot-2`,
      mods: `aeroflot-2, shadow-right`
    },
    {
      icon: `aurora`,
      mods: `aurora`
    },
    {
      icon: `eastjet`,
      mods: `eastjet`
    },
    {
      icon: `s7`,
      mods: `s7`
    }
  ];


  constructor(private localStorage: LocalStorageService) { }

  ngOnInit(): void {
    this.phoneNumber = this.localStorage.get('phone-number');
  }
}
