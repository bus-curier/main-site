import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FromAirportPageComponent} from './from-airport-page.component';
import {SharedModule} from '../../../shared/shared.module';



@NgModule({
  declarations: [
    FromAirportPageComponent
  ],
  imports: [
    CommonModule,
    SharedModule
  ]
})
export class FromAirportPageModule { }
