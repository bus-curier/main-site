import {AfterViewInit, Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import pickupPoints from '../../../../mock-data/pickup-points';
import dropdown from '../../../../core/animations/dropdown';
import {animate, keyframes, state, style, transition, trigger} from '@angular/animations';
import media from '../../../../core/utils/media';
import { ContactsService } from 'src/app/pages/contacts-page/services/contacts/contacts.service';
import {Subscription} from 'rxjs';
import {Office} from '../../../../core/interfaces/calculator';
import {map, take} from 'rxjs/operators';
import {UtilService} from 'angular-mydatepicker';
import { FormUtilsService } from 'src/app/core/services/form-utils.service';

const points = ['IML', 'HERMES', 'CSE', 'Boxberry'];

@Component({
  selector: 'app-pick-up-tabs',
  templateUrl: './pick-up-tabs.component.html',
  styleUrls: ['./pick-up-tabs.component.scss'],
  animations: [
    trigger('activeTab', [
      transition('void => *', [
        // style({opacity: 0, height: '0px'}),
        style({opacity: 0}),
        animate('300ms')
      ])
    ])
  ]
})
export class PickUpTabsComponent implements OnInit {
  @ViewChild('tabs', {read: ElementRef}) tabs: ElementRef;

  public tabsMap = {
    BoxBerry: 'BoxBerry',
    IML: 'IML',
    Hermes: 'Hermes',
    KCE: 'Kce'
  };

  public pickupPoints = pickupPoints;
  public activeTab = '';
  public activeTabNode = null;
  public breakpoint = null;
  public minWidthMD = false;

  public points = {};
  public currentCityPoint = null;
  private sub: Subscription;

  constructor(
    public utils: FormUtilsService,
    public contactsService: ContactsService) { }

  ngOnInit(): void {
    this.breakpoint = window.matchMedia(`(min-width: ${media.MD}px)`);
    this.breakpoint.addListener(this.checkScreen.bind(this));
    this.checkScreen();

    this.contactsService.getOffices()
      .pipe(
        take(1),
        map((offices: any) => {
          return offices.filter((office) => {
            return office.pvz;
          });
        }),
        map((offices: any) => {
          return points.reduce((obj, point: string) => {
            return {
              ...obj,
              [point]: offices.filter((office) => JSON.parse(office.pvz)[point])
            };
          }, {});
        })
      )
      .subscribe((obj: any) => {
        this.points = obj;
        this.activeTab = Object.keys(obj)[0];
      });
  }

  checkScreen() {
    this.minWidthMD = this.breakpoint && this.breakpoint.matches;
  }

  showTab(tab, btn) {
    this.activeTab = tab;
    this.activeTabNode = btn;
    this.scrollTo(btn);
  }

  onPickupChange(point: any) {
    this.currentCityPoint = point;

    if (this.activeTabNode) {
      this.scrollTo(this.activeTabNode);
    } else {

      // This function fires first time, when component\page init,
      // that is why we can init activeTabNode here
      this.activeTabNode = this.tabs.nativeElement.children[0];
    }
  }

  scrollTo(el) {
    if (!el) {
      return;
    }

    setTimeout(() => {
      const top = Math.floor(el.getBoundingClientRect().top + window.pageYOffset - 85);

      window.scrollTo({
        top,
        behavior: 'smooth'
      });
    }, 0);
  }
}
