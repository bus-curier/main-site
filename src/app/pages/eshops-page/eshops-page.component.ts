import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-eshops-page',
  templateUrl: './eshops-page.component.html',
  styleUrls: ['./eshops-page.component.scss']
})
export class EshopsPageComponent implements OnInit {

  public shops = [
    {
      icon: `shopping-live`,
      mods: `shopping-live, shadow-right`
    },
    {
      icon: `decathlon`,
      mods: `decathlon`
    },
    {
      icon: `zara`,
      mods: `zara`
    },
    {
      icon: `child-world`,
      mods: `child-world`
    }
  ];

  public pickPoints = [
    {
      icon: `iml`,
      mods: `iml`
    },
    {
      icon: `boxberry`,
      mods: `boxberry`
    },
    {
      icon: `hermes`,
      mods: `hermes`
    },
    {
      icon: `kce`,
      mods: `kce`
    }
  ];

  constructor() { }

  ngOnInit(): void {
  }

}
