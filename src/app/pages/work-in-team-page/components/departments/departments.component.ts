import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-departments',
  templateUrl: './departments.component.html',
  styleUrls: ['./departments.component.scss']
})
export class DepartmentsComponent implements OnInit {
  public imagesPath = '/assets/images/pages/work-in-team/components/departments/images';

  constructor() { }

  ngOnInit(): void {
  }

}
