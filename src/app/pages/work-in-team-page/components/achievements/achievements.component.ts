import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-achievements',
  templateUrl: './achievements.component.html',
  styleUrls: ['./achievements.component.scss']
})
export class AchievementsComponent implements OnInit {
  public imagesPath = '/assets/images/pages/work-in-team/components/achievements/images';

  constructor() { }

  ngOnInit(): void {
  }

}
