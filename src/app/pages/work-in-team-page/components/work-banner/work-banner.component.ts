import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-work-banner',
  templateUrl: './work-banner.component.html',
  styleUrls: ['./work-banner.component.scss']
})
export class WorkBannerComponent implements OnInit {
  public imagesPath = '/assets/images/pages/work-in-team/components/work-banner/images';

  constructor() { }

  ngOnInit(): void {
  }

}
