import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-promo',
  templateUrl: './promo.component.html',
  styleUrls: ['./promo.component.scss']
})
export class PromoComponent implements OnInit {
  public imagesPath = '/assets/images/pages/index/components/promo/images';

  constructor() { }

  ngOnInit(): void {
    console.log('this', this);
  }

}
