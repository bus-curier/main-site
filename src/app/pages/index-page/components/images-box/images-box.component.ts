import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-images-box',
  templateUrl: './images-box.component.html',
  styleUrls: ['./images-box.component.scss']
})
export class ImagesBoxComponent implements OnInit {
  public imagesPath = '/assets/images/pages/index/components/images-box/images';

  constructor() { }

  ngOnInit(): void {
  }

}
