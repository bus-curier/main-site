import { Component, OnInit } from '@angular/core';
import FormControlName from 'src/app/core/maps/FormControlName';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {UtilsService} from '../../../../core/services/utils.service';
import { CommonService } from 'src/app/core/services/common/common.service';
import {SimpleModalService} from 'ngx-simple-modal';
import {Pattern} from '../../../../core/pattern/pattern';
import {Router} from '@angular/router';
import {BaseFormComponent} from '../../../../shared/components/base-form/base-form.component';

@Component({
  selector: 'app-form-task',
  templateUrl: './form-task.component.html',
  styleUrls: ['./form-task.component.scss']
})
export class FormTaskComponent extends BaseFormComponent implements OnInit {

  constructor(
    public utils: UtilsService,
    public simpleModal: SimpleModalService,
    public router: Router,
    public commonService: CommonService) {
    super(simpleModal, router, commonService);
  }

  ngOnInit(): void {
    this.form = new FormGroup({
      [FormControlName.Fio]: new FormControl('',
        [Validators.required, Validators.pattern(Pattern.Text),
          Validators.minLength(2)]),
      [FormControlName.Tel]: new FormControl('',
        [Validators.required, Validators.pattern(Pattern.Phone)]),
      [FormControlName.Email]: new FormControl('',
        [Validators.required, Validators.email]),
      [FormControlName.Question]: new FormControl('',
        [Validators.required, Validators.pattern(Pattern.TextWithNumbersAndSymbols),
        Validators.minLength(2)]),
      [FormControlName.Agree]: new FormControl(false, [Validators.required]),
      [FormControlName.Captcha]: new FormControl('', [Validators.required])
    });
  }

  onSubmit() {
    const data = this.form.value;

    super.onSubmit({
      sender: data.fio,
      phone: data.tel,
      email: '',
      message: data.message
    });
  }
}
