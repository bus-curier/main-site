import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PromoComponent } from './components/promo/promo.component';
import {IndexPageComponent} from './index-page.component';
import {SharedModule} from '../../shared/shared.module';
import { CoverageComponent } from './components/coverage/coverage.component';
import { ServicesComponent } from './components/services/services.component';
import { FormTaskComponent } from './components/form-task/form-task.component';
import { FormCitiesComponent } from './components/form-cities/form-cities.component';
import { ExamplesComponent } from './components/examples/examples.component';
import { PartnersComponent } from './components/partners/partners.component';
import { ImagesBoxComponent } from './components/images-box/images-box.component';
import { ExampleComponent } from './components/example/example.component';



@NgModule({
  declarations: [
    IndexPageComponent,
    PromoComponent,
    CoverageComponent,
    ServicesComponent,
    FormTaskComponent,
    FormCitiesComponent,
    ExamplesComponent,
    PartnersComponent,
    ImagesBoxComponent,
    ExampleComponent
  ],
  imports: [
    CommonModule,
    SharedModule
  ]
})
export class IndexPageModule { }
