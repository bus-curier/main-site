import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AboutComponent } from './components/about/about.component';
import {SharedModule} from '../../shared/shared.module';
import { AdvantagesComponent } from './components/advantages/advantages.component';
import { SocialComponent } from './components/social/social.component';
import { TeamComponent } from './components/team/team.component';
import {AboutPageComponent} from './about-page.component';
import { AchievementsComponent } from './components/achievements/achievements.component';



@NgModule({
  declarations: [
    AboutPageComponent,
    AboutComponent,
    AdvantagesComponent,
    SocialComponent,
    TeamComponent,
    AchievementsComponent
  ],
  imports: [
    CommonModule,
    SharedModule
  ]
})
export class AboutPageModule { }
