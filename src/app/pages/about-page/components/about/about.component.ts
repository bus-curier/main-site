import { Component, OnInit } from '@angular/core';

const list = [
  'Скорость – доставка от 2 до 24 часов.',
  'Доставка в труднодоступные населенные пункты в рамках региона, области, края.',
  'Транспортировка различных категорий груза, за исключением опасных и запрещенных к перевозке.',
  'Гарантия отправки в короткие сроки – регулярное движение междугородних маршрутов.',
  'Удобство получения груза – гибкая система выдачи груза.'
];

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent implements OnInit {
  public imagesPath = '/assets/images/pages/about/components/about/images';
  public list = list;

  constructor() { }

  ngOnInit(): void {
  }

}
