import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {OrderFormModule} from './order-form/order-form.module';
import {NewOrderComponent} from './new-order/new-order.component';
import {SharedModule} from '../../../shared/shared.module';



@NgModule({
  declarations: [
    NewOrderComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    OrderFormModule
  ],
  exports: [
    NewOrderComponent,
    OrderFormModule,
    SharedModule
  ]
})
export class OrdersCommonModule { }
