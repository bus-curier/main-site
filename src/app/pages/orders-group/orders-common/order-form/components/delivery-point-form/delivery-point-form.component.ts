import {Component, forwardRef, Input, OnChanges, OnDestroy, OnInit, SimpleChanges} from '@angular/core';
import formFieldMeta from '../../../../../../core/form/formFieldMeta';
import fieldError from '../../../../../../core/form/fieldError';
import {AbstractControl, FormControl, FormGroup, NG_VALIDATORS, NG_VALUE_ACCESSOR, Validators} from '@angular/forms';
import {FormUtilsService} from '../../../../../../core/services/form-utils.service';
import {UtilsService} from '../../../../../../core/services/utils.service';
import {CalculatorService} from '../../../../../../core/services/calculator/calculator.service';
import {SubFormComponent} from '../sub-form/sub-form.component';
import FormControlName from 'src/app/core/maps/FormControlName';
import fadeIn from '../../../../../../core/animations/fadeIn';
import {combineAll, concatAll, debounceTime, delay, first, map, take, tap} from 'rxjs/operators';
import {BehaviorSubject, combineLatest, Subscription, zip} from 'rxjs';
import {ActivatedRoute, Params} from '@angular/router';
import {CityTo} from '../../../../../../core/interfaces/calculator';
import {Select} from '../../../../../../core/interfaces/form';
import {LocalStorageService} from '../../../../../../core/services/local-storage.service';
import {VLOffice} from '../../../../../../core/maps/calculator';

@Component({
  selector: 'app-delivery-point-form',
  templateUrl: './delivery-point-form.component.html',
  styleUrls: ['./delivery-point-form.component.scss'],
  animations: [fadeIn],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => DeliveryPointFormComponent),
      multi: true
    },
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => DeliveryPointFormComponent),
      multi: true
    }
  ]
})
export class DeliveryPointFormComponent extends SubFormComponent implements OnInit, OnDestroy {
  @Input() noLabel: boolean;

  public TabName = {
    get: 'Забрать в отделении',
    delivery: 'Вызвать курьера',
    'need-to-meet': 'Встретить с автобуса'
  };

  public isFormGroupDisabled = false;

  public cities = [];
  public departments = [];
  public cityData = {};

  public cityFromLoaded = false;

  private citiesSub: Subscription;
  private departmentsSub: Subscription;

  public offices$ = new BehaviorSubject([]);
  private officesSub: Subscription;
  private officesByIdSub: Subscription;
  private combineSub: Subscription;


  constructor(public formUtils: FormUtilsService,
              public utils: UtilsService,
              private route: ActivatedRoute,
              private localStorage: LocalStorageService,
              private calcService: CalculatorService) {
    super();
  }

  ngOnInit(): void {
    this.formGroup = new FormGroup({
      [FormControlName.Location]: new FormControl({value: '', disabled: false}, [Validators.required]),
      [FormControlName.Options]: new FormGroup({
        [FormControlName.Active]: new FormControl('', [Validators.required]),
      })
    });

    this.loadOffices();
  }

  get options() {
    return this.formGroup.get(FormControlName.Options) as FormGroup;
  }

  loadCities(id) {
    return this.calcService.getCityTo(id, 0)
      .pipe(
        tap((cities) => {
          this.localStorage.set('citiesTo', cities);
        }),
        map<CityTo, Select>((cities: any) => {
          return cities
            .map((city) => {
              this.cityData[city.id] = city;
              return {value: city.id, name: city.name};
          });
        }),
        map((cities: any) => {
          const sorted = cities.sort((a: Select, b: Select) => {
            return a.name.localeCompare(b.name);
          });

          return [{value: '', name: 'Выберите город'}, ...sorted];
        }),
        tap((cities: any) => {
          this.cities = cities;
        }),
        delay(0)
    );
  }

  loadOffices() {
    this.officesSub = this.calcService.getOffices()
      .subscribe((arr: any) => {
        this.offices$.next(arr);
      });
  }

  getOfficesById(id) {
    return this.offices$
      .pipe(
        map((offices: any) => {
          return offices.filter((office) => office.office_id === this.cityData[id].office_id);
        })
      );
  }

  setCity(id: string) {
    this.createTabs(id);
    this.createNeedToMeetControl(id);
  }

  createTabs(id: string) {
    this.officesByIdSub = this.getOfficesById(id)
      .pipe(
        tap(() => {
          this.clearOptions();
        }),
        concatAll(),
        first(),
        map((office: any) => {
          return Object.entries(office)
            .filter((item: [string, any]) => {
              return item[0] === 'get' && item[1] === '1' || item[0] === 'delivery' && item[1] === '1';
            })
            .map((item: [string, any]) => {
              return item[0];
            });
        })
      )
      .subscribe((tabs: string[]) => {
        if (tabs.length) {
          tabs.forEach((name: string) => {
            this.options.addControl(name, new FormControl(''));
          });

          this.options.get(FormControlName.Active).setValue(tabs[0]);
        }
      },
        () => {},
     () => {
        const departmentControl = this.options.get(FormControlName.Get);

        if (departmentControl) {
          this.getDepartments(id);
        }
      });
  }

  getDepartments(id: string) {
    this.getOfficesById(id)
      .pipe(
        take(1),
        map((offices: any) => {
          return offices
            .filter((office) => +office.get)
            .map((office) => {
              return {
                value: office.home_id || office.office_id,
                name: office.address,
                data: {
                  coords: {geo_x: office.geo_x, geo_y: office.geo_y}
                }
              };
            });
        })
      )
      .subscribe((offices: any) => {
        this.departments = offices;
      });
  }

  createNeedToMeetControl(id) {
    if (this.cityData[id].need_to_meet !== '0') {
      this.options.addControl(FormControlName.NeedToMeet, new FormControl(''));

      const active = this.options.get(FormControlName.Active);

      if (this.findControl(FormControlName.NeedToMeet) && !this.findControl(FormControlName.Get)) {
        active.setValue(FormControlName.NeedToMeet);
      }

    } else {
      this.options.removeControl(FormControlName.NeedToMeet);
    }
  }

  changeType(type: string) {
    Object.entries(this.options.controls)
      .forEach(([key, control]: [string, AbstractControl]) => {
        if (key !== FormControlName.Active) {
          control.clearValidators();
          control.setValue('');
        }
      });

    this.options.get(type).setValidators([Validators.required]);

    // После того как форма Department или Courier была выбрана обновляем ее значения,
    // чтобы общая форма показала статус invalid при необходимости
    setTimeout(() => {
      this.options.get(type).updateValueAndValidity();
    }, 0);
  }

  clearOptions() {
    Object.entries(this.options.controls)
      .forEach(([key, control]: [string, AbstractControl]) => {
        if (key !== FormControlName.Active) {
          this.options.removeControl(key);
        }
      });
  }

  findControl(name) {
    return Object.keys(this.options.controls).indexOf(name) !== -1;
  }

  writeValue(value: any): void {
    this.combineSub = combineLatest(
      this.route.queryParams,
      this.calcService.cityFromId$)
      .pipe(
        delay(0),
        debounceTime(0)
      )
      .subscribe(([params, id]) => {
        const cityFromId = params.cityFromId || id;
        const cityToId = params.cityToId || (value && value.location);

        console.log('cityFromId', id);

        if (!cityFromId) {
          return;
        }

        this.cityFromLoaded = true;

        this.loadCities(cityFromId)
          .pipe(delay(0))
          .subscribe((cities: any) => {
            if (cityToId) {
              this.formGroup.get(FormControlName.Location).setValue(cityToId);
              this.setCity(cityToId);
            } else {
              this.formGroup.get(FormControlName.Location).setValue(cities[0].value);
            }

            super.writeValue(value);
            // this.clearOptions();
          });
      });
  }

  ngOnDestroy(): void {
    if (this.citiesSub) {
      this.citiesSub.unsubscribe();
    }

    if (this.officesSub) {
      this.officesSub.unsubscribe();
    }

    if (this.officesByIdSub) {
      this.officesByIdSub.unsubscribe();
    }

    if (this.departmentsSub) {
      this.departmentsSub.unsubscribe();
    }

    if (this.combineSub) {
      this.combineSub.unsubscribe();
    }
  }
}
