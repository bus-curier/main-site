import {Component, EventEmitter, forwardRef, Input, OnInit, Output, SimpleChanges} from '@angular/core';
import {FormControl, NG_VALUE_ACCESSOR} from '@angular/forms';
import fadeIn from '../../../../../../core/animations/fadeIn';
import {ValueAccessorComponent} from '../../../../../../shared/components/value-accessor/value-accessor.component';

@Component({
  selector: 'app-package-modal',
  templateUrl: './package-modal.component.html',
  styleUrls: ['./package-modal.component.scss'],
  animations: [fadeIn],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => PackageModalComponent),
      multi: true
    }
  ]
})
export class PackageModalComponent extends ValueAccessorComponent implements OnInit {
  @Input() data;
  @Input() type;
  @Input() title: string;

  @Output() close: EventEmitter<any> = new EventEmitter<any>();
  @Output() ok: EventEmitter<any> = new EventEmitter<any>();

  public counter: FormControl;

  public Package = {
    box: 'Коробка',
    ['safe-pack']: 'Сейф-пакет',
    ['plastic-pack']: 'Полиэтиленовый пакет',
    skins: 'Пленка',
    other: 'Другое'
  };

  constructor() {
    super();
  }

  ngOnInit(): void {
    this.cssClass = this.setClass('package-modal');
    this.counter = new FormControl(0);
  }

  onOk() {
    this.changeValue(this.counter.value);
    this.ok.emit(this.counter.value);
  }

  onClose() {
    this.close.emit();
  }

  writeValue(value) {
    this.counter.setValue(value || 1);
  }
}
