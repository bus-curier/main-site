import {AfterViewInit, Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {OrderService} from '../../../../core/services/order/order.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import FormControlName from 'src/app/core/maps/FormControlName';
import {OrderTracking} from '../../../../core/interfaces/order';
import {delay, take, tap} from 'rxjs/operators';
import {ActivatedRoute, Params} from '@angular/router';

@Component({
  selector: 'app-track-order-page',
  templateUrl: './track-order-page.component.html',
  styleUrls: ['./track-order-page.component.scss']
})
export class TrackOrderPageComponent implements OnInit, AfterViewInit {
  @ViewChild('container', {read: ElementRef}) container: ElementRef;
  @ViewChild('formContainer', {read: ElementRef}) formContainer: ElementRef;

  public imagesPath = '/assets/images/pages/orders-group/track-order-page/images';
  public FormControlName = FormControlName;

  public form: FormGroup;
  public orderNumber = null;
  public orderData: OrderTracking[] = [];
  public isLoading = false;
  public error = false;

  constructor(
    private orderService: OrderService,
    public route: ActivatedRoute) { }

  ngOnInit(): void {
    this.form = new FormGroup({
      [FormControlName.OrderNumber]: new FormControl('', [Validators.required])
    });
  }

  ngAfterViewInit(): void {

    this.route.queryParams
      .pipe(
        take(1),
        delay(0),
        tap((params: Params) => {
          if (params.id) {
            this.form.get(FormControlName.OrderNumber).setValue(params.id);
            this.scrollTo();
          }
        }),
        delay(1000)
      )
      .subscribe((params: Params) => {
        if (params.id) {
          this.onSubmit();
        }
      });
  }

  onSubmit() {
    if (!this.form.valid) {
      return;
    }

    this.isLoading = true;
    this.orderNumber = this.form.value[FormControlName.OrderNumber];

    this.orderService.getTracking(this.orderNumber)
      .pipe(take(1))
      .subscribe((data: OrderTracking[]) => {
        if (data) {
          this.orderData = data;
          this.error = false;
          this.isLoading = false;
        }
      }, (err) => {
          this.error = true;
          this.orderData = [];
          this.orderNumber = '';
          this.isLoading = false;
      });
  }

  scrollTo() {
    const top = Math.floor(this.container.nativeElement.getBoundingClientRect().top);

    window.scrollTo({
      top,
      behavior: 'smooth'
    });
  }
}
