import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {QuickOrderPageComponent} from './quick-order-page.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { FailPageComponent } from './pages/fail-page/fail-page.component';
import { DonePageComponent } from './pages/done-page/done-page.component';
import { IndexPageComponent } from './pages/index-page/index-page.component';
import {QuickOrderRoutingModule} from './quick-order-routing.module';
import {OrdersCommonModule} from '../../orders-common/orders-common.module';

@NgModule({
  declarations: [
    QuickOrderPageComponent,
    SidebarComponent,
    FailPageComponent,
    DonePageComponent,
    IndexPageComponent
  ],
  imports: [
    CommonModule,
    OrdersCommonModule,
    QuickOrderRoutingModule
  ]
})
export class QuickOrderPageModule { }
