import { Component, OnInit } from '@angular/core';
import {SimpleModalService} from 'ngx-simple-modal';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {LoginModalComponent} from '../../../../../../modals/login-modal/login-modal.component';
import {take} from 'rxjs/operators';

@Component({
  selector: 'app-done-page',
  templateUrl: './done-page.component.html',
  styleUrls: ['./done-page.component.scss']
})
export class DonePageComponent implements OnInit {
  public imagesPath = '/assets/images/pages/orders-group/order-page/done/images';
  public id = '';

  constructor(
    private modalService: SimpleModalService,
    private route: ActivatedRoute,
    private router: Router) { }

  ngOnInit(): void {
    this.route.params
      .pipe(take(1))
      .subscribe((params: Params) => {
        this.id = params.id;
      });
  }

  showLoginModal() {
    // e.preventDefault();
    this.modalService.addModal(LoginModalComponent);
  }

  goToIndex() {
    this.router.navigate(['/']);
  }

  goToOrderStatus() {
    this.router.navigate(['orders', 'track-order'],
      { queryParams: { id: this.id}});
  }
}
