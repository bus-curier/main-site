import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {OrderPageComponent} from './order-page.component';
import {ModalsModule} from '../../../../modals/modals.module';
import {SidebarComponent} from './components/sidebar/sidebar.component';
import { DonePageComponent } from './pages/done-page/done-page.component';
import { IndexPageComponent } from './pages/index-page/index-page.component';
import {OrderRoutingModule} from './order-routing.module';
import { FailPageComponent } from './pages/fail-page/fail-page.component';
import {OrdersCommonModule} from '../../orders-common/orders-common.module';
import {OrderStepsComponent} from './components/order-steps/order-steps.component';

@NgModule({
  declarations: [
    OrderPageComponent,
    SidebarComponent,
    OrderStepsComponent,
    DonePageComponent,
    IndexPageComponent,
    FailPageComponent
  ],
  imports: [
    CommonModule,
    ModalsModule,
    OrdersCommonModule,
    OrderRoutingModule
  ]
})
export class OrderPageModule { }
