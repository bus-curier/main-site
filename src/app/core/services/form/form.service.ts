import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {FormEndpointService} from './form-endpoint.service';
import {Observable} from 'rxjs';
import {CityFrom, Office} from '../../interfaces/calculator';

@Injectable({
  providedIn: 'root'
})

export class FormService {

  constructor(private http: HttpClient,
              private endpoint: FormEndpointService) { }

  getCitiesFrom(): Observable<CityFrom> {
    return this.endpoint.getCitiesFrom();
  }

  getDistricts(id) {
    return this.endpoint.getDistricts(id);
  }

  getCityTo(cityId, districtId) {
    return this.endpoint.getCityTo(cityId, districtId);
  }

  getTypes(cityFromId, cityToId) {
    return this.endpoint.getTypes(cityFromId, cityToId);
  }

  getServices(id) {
    return this.endpoint.getServices(id);
  }

  getOffices(): Observable<Office> {
    return this.endpoint.getOffices();
  }
}
