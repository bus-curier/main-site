import {Injectable, Injector} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {CityFrom, Office} from '../../interfaces/calculator';
import {EndpointFactory} from '../endpoint-factory/endpoint-factory.service';

@Injectable({
  providedIn: 'root'
})
export class FormEndpointService extends EndpointFactory {
  private readonly _url: string = '/calc';

  get url() { return this.baseUrl + this._url; }

  constructor(http: HttpClient, injector: Injector) {
    super(http, injector);
  }

  getCitiesFrom(): Observable<CityFrom> {
    return this.execute(this.http.get<CityFrom>(`${this.url}/getcitiesfrom`, this.getRequestHeaders()),
      () => this.getCitiesFrom());
  }

  getDistricts<T>(id: string): Observable<T> {
    return this.execute(this.http.get<T>(`${this.url}/getdistricts/${id}`, this.getRequestHeaders()),
      () => this.getDistricts(id));
  }

  getCityTo<T>(cityId: string, distrId: string): Observable<T> {
    return this.execute(this.http.get<T>(`${this.url}/getcityto/${cityId}/${distrId}`, this.getRequestHeaders()),
      () => this.getCityTo(cityId, distrId));
  }

  getTypes<T>(cityFromId: string, cityToId: string): Observable<T> {
    return this.execute(this.http.get<T>(`${this.url}/gettypes/${cityFromId}/${cityToId}`, this.getRequestHeaders()),
      () => this.getTypes(cityFromId, cityToId));
  }

  getServices<T>(id: string): Observable<T> {
    return this.execute(this.http.get<T>(`${this.url}/getservices/${id}`, this.getRequestHeaders()),
      () => this.getServices(id));
  }

  getOffices(): Observable<Office> {
    return this.execute(this.http.get<Office>(`${this.url}/getoffices`, this.getRequestHeaders()),
      () => this.getOffices());
  }
}
