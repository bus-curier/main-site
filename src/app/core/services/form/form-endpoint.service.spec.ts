import { TestBed } from '@angular/core/testing';

import { FormEndpointService } from './form-endpoint.service';

describe('FormEndpointService', () => {
  let service: FormEndpointService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FormEndpointService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
