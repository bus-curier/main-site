import { NgModule } from '@angular/core';
import {Routes, RouterModule, PreloadAllModules, ExtraOptions} from '@angular/router';
import {IndexPageComponent} from './pages/index-page/index-page.component';
import {EshopsPageComponent} from './pages/eshops-page/eshops-page.component';
import {DocumentsPageComponent} from './pages/documents-page/documents-page.component';
import {WorkInTeamPageComponent} from './pages/work-in-team-page/work-in-team-page.component';
import {FeedbackPageComponent} from './pages/feedback-page/feedback-page.component';
import {AboutPageComponent} from './pages/about-page/about-page.component';
import {ContactsPageComponent} from './pages/contacts-page/contacts-page.component';
import {NotFoundPageComponent} from './pages/not-found-page/not-found-page.component';

const routerOptions: ExtraOptions = {
  // scrollPositionRestoration: 'enabled',
  // anchorScrolling: 'enabled',
  scrollOffset: [0, 0],
  scrollPositionRestoration: 'enabled',
  preloadingStrategy: PreloadAllModules
  // onSameUrlNavigation: 'reload'
};

const routes: Routes = [
  {path: '', component: IndexPageComponent, data: { title: 'Главная' }},
  {path: 'eshops', component: EshopsPageComponent, data: { title: 'Интернет-магазины' }},
  {path: 'documents', component: DocumentsPageComponent, data: { title: 'Документы' }},
  {path: 'work-in-team', component: WorkInTeamPageComponent, data: { title: 'Работа в команде' }},
  {path: 'feedback', component: FeedbackPageComponent, data: { title: 'Обратная связь' }},
  {path: 'about', component: AboutPageComponent},
  {path: 'contacts', component: ContactsPageComponent, data: { title: 'Контакты' }},
  {path: 'services', loadChildren: () => import('./pages/services-group/services-group.module').then((m) => m.ServicesGroupModule)},
  {path: 'info', loadChildren: () => import('./pages/info-group/info-group.module').then((m) => m.InfoGroupModule)},
  {path: 'orders', loadChildren: () => import('./pages/orders-group/orders-group.module').then((m) => m.OrdersGroupModule)},
  {path: 'account', loadChildren: () => import('./pages/account-page/account-page.module').then((m) => m.AccountPageModule)},
  {path: '**', component: NotFoundPageComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes, routerOptions)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
