import {Component, Input, OnInit} from '@angular/core';
import {BaseComponent} from '../base/base.component';

@Component({
  selector: 'app-info-box',
  templateUrl: './info-box.component.html',
  styleUrls: ['./info-box.component.scss']
})
export class InfoBoxComponent extends BaseComponent implements OnInit {
  @Input() title: string;

  constructor() {
    super();
  }

  ngOnInit(): void {
    this.cssClass = this.setClass('info-box');
  }
}
