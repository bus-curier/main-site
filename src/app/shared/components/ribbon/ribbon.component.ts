import { Component, OnInit } from '@angular/core';
import {BaseComponent} from '../base/base.component';

@Component({
  selector: 'app-ribbon',
  templateUrl: './ribbon.component.html',
  styleUrls: ['./ribbon.component.scss']
})
export class RibbonComponent extends BaseComponent implements OnInit {

  constructor() {
    super();
  }

  ngOnInit(): void {
    this.cssClass = this.setClass('ribbon');
  }
}
