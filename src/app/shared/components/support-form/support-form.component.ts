import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {UtilsService} from '../../../core/services/utils.service';
import FormControlName from 'src/app/core/maps/FormControlName';
import {FormUtilsService} from '../../../core/services/form-utils.service';
import {SimpleModalService} from 'ngx-simple-modal';
import { CommonService } from 'src/app/core/services/common/common.service';
import {Pattern} from '../../../core/pattern/pattern';
import {Router} from '@angular/router';
import {BaseFormComponent} from '../base-form/base-form.component';

@Component({
  selector: 'app-support-form',
  templateUrl: './support-form.component.html',
  styleUrls: ['./support-form.component.scss']
})
export class SupportFormComponent extends BaseFormComponent implements OnInit {

  constructor(public formUtils: FormUtilsService,
              public simpleModal: SimpleModalService,
              public commonService: CommonService,
              public router: Router,
              public utils: UtilsService) {
    super(simpleModal, router, commonService);
  }

  ngOnInit(): void {
    this.form = new FormGroup({
      [FormControlName.FirstName]: new FormControl('',
        [Validators.required, Validators.pattern(Pattern.Text),
          Validators.minLength(2)]),
      [FormControlName.Tel]: new FormControl('',
        [Validators.required, Validators.pattern(Pattern.Phone)]),
      [FormControlName.Email]: new FormControl('',
        [Validators.required, Validators.email]),
      [FormControlName.Question]: new FormControl('',
        [Validators.required,  Validators.pattern(Pattern.TextWithNumbersAndSymbols),
        Validators.minLength(2)]),
      [FormControlName.Agree]: new FormControl(false, [Validators.required]),
      [FormControlName.Captcha]: new FormControl('', [Validators.required])
    });
  }

  onSubmit() {
    const data = this.form.value;

    super.onSubmit({
      sender: data.firstName,
      phone: data.tel,
      email: data.email,
      message: data.message
    });
  }
}
