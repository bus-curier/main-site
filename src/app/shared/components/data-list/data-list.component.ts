import {Component, Input, OnInit} from '@angular/core';
import {BaseComponent} from '../base/base.component';

@Component({
  selector: 'app-data-list',
  templateUrl: './data-list.component.html',
  styleUrls: ['./data-list.component.scss']
})
export class DataListComponent extends BaseComponent implements OnInit {
  @Input() data;

  constructor() {
    super();
  }

  ngOnInit(): void {
    this.cssClass = this.setClass('data-list');
  }
}
