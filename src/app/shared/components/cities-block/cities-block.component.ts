import {Component, Input, OnInit} from '@angular/core';
import {BaseComponent} from '../base/base.component';

@Component({
  selector: 'app-cities-block',
  templateUrl: './cities-block.component.html',
  styleUrls: ['./cities-block.component.scss']
})
export class CitiesBlockComponent extends BaseComponent implements OnInit {
  @Input() letter: string;
  @Input() names: any;

  constructor() {
    super();
  }

  ngOnInit(): void {
    this.cssClass = this.setClass('cities-block');
  }
}
