import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-base',
  templateUrl: './base.component.html',
  styleUrls: ['./base.component.scss']
})
export class BaseComponent implements OnInit {
  @Input() mods;
  @Input() invalid;
  @Input() disabled = false;

  public cssClass;

  constructor() { }

  ngOnInit(): void {}

  setClass(cls, mods = this.mods) {
    return this.setMods(cls, mods);
  }

  private setMods(cls, mods) {
    let cssClass = cls;
    let allMods = '';

    if (mods !== 'undefined' && mods ) {
      const modsList = mods.split(',');
      for (const item of modsList) {
        allMods = allMods + ` ${cls}--` + item.trim();
      }
    }

    cssClass += allMods;

    return cssClass;
  }
}
