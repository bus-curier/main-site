import {Component, Input, OnInit} from '@angular/core';
import {ModsService} from '../../../core/services/mods.service';
import {BaseComponent} from '../base/base.component';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent extends BaseComponent implements OnInit {
  @Input() head: [];
  @Input() body: [];
  @Input() headerMods;
  @Input() withScroll;

  public tableClass = '';
  public headerClass = '';

  constructor() {
    super();
  }

  ngOnInit(): void {
    this.tableClass = this.setClass('table');
    this.headerClass = this.setClass('table__header', this.headerMods);
  }
}
