import {Component, Input, OnInit} from '@angular/core';
import {BaseComponent} from '../base/base.component';

// svg
import './svg/location-arrow.svg';
import './svg/header-logo.svg';
import './svg/footer-logo.svg';
import './svg/user-logged.svg';
import './svg/user-unlogged.svg';
import './svg/email.svg';
import './svg/help.svg';
import './svg/help-circle.svg';
import './svg/info.svg';
import './svg/minus.svg';
import './svg/phone.svg';
import './svg/plus.svg';
import './svg/instagram.svg';
import './svg/box.svg';
import './svg/docs.svg';
import './svg/parcel.svg';
import './svg/aeroflot.svg';
import './svg/hyperauto.svg';
import './svg/unilab.svg';
import './svg/sber.svg';
import './svg/disk.svg';
import './svg/from-airport.svg';
import './svg/sms.svg';
import './svg/storage.svg';
import './svg/to-box.svg';
import './svg/to-door.svg';
import './svg/asos.svg';
import './svg/boxberry.svg';
import './svg/child-world.svg';
import './svg/decathlon.svg';
import './svg/hermes.svg';
import './svg/iherb.svg';
import './svg/iml.svg';
import './svg/kce.svg';
import './svg/ozon.svg';
import './svg/shopping-live.svg';
import './svg/toys.svg';
import './svg/zara.svg';
import './svg/get.svg';
import './svg/give.svg';
import './svg/location.svg';
import './svg/office.svg';
import './svg/back.svg';
import './svg/point.svg';
import './svg/alarm.svg';
import './svg/union.svg';
import './svg/headphone.svg';
import './svg/heart.svg';
import './svg/services.svg';
import './svg/stairs.svg';
import './svg/team.svg';
import './svg/time.svg';
import './svg/house.svg';
import './svg/man.svg';
import './svg/check.svg';
import './svg/geo.svg';
import './svg/home-point.svg';
import './svg/schedule.svg';
import './svg/track.svg';
import './svg/aeroflot-2.svg';
import './svg/aurora.svg';
import './svg/eastjet.svg';
import './svg/s7.svg';
import './svg/attention.svg';
import './svg/thanks.svg';
import './svg/done.svg';
import './svg/delete.svg';
import './svg/whatsapp.svg';
import './svg/phone-help.svg';
import './svg/arrow-right-bold.svg';
import './svg/pdf.svg';
import './svg/more.svg';
import './svg/ok.svg';
import './svg/notice.svg';
import './svg/files.svg';
import './svg/orders.svg';
import './svg/profile.svg';
import './svg/report.svg';
import './svg/full-screen.svg';
import './svg/close.svg';
import './svg/burger.svg';
import './svg/map.svg';


@Component({
  selector: 'app-icon',
  templateUrl: './icon.component.html',
  styleUrls: ['./icon.component.scss']
})
export class IconComponent extends BaseComponent  implements OnInit {
  @Input() width: number;
  @Input() height: number;
  @Input() name: string;

  constructor() {
    super();
  }

  ngOnInit(): void {
    this.cssClass = this.setClass('icon');
  }
}
