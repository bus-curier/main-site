import {
  ChangeDetectorRef,
  Component,
  ElementRef,
  EventEmitter,
  forwardRef,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
  ViewChild
} from '@angular/core';
import {ModsService} from '../../../core/services/mods.service';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from '@angular/forms';
import State from '../../../core/maps/State';
import {DeviceDetectorService} from 'ngx-device-detector';
import {ValueAccessorComponent} from '../value-accessor/value-accessor.component';

@Component({
  selector: 'app-counter',
  templateUrl: './counter.component.html',
  styleUrls: ['./counter.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => CounterComponent),
      multi: true
    }
  ]
})
export class CounterComponent extends ValueAccessorComponent implements OnInit, OnChanges {
  @ViewChild('input', {read: ElementRef}) input: ElementRef;

  @Output() change: EventEmitter<any> = new EventEmitter<any>();

  public currentCount = 0;

  constructor(
    private cdr: ChangeDetectorRef,
    public device: DeviceDetectorService) {
    super();
  }

  ngOnInit(): void {

    if (this.value > 0) {
      this.currentCount = this.value;
    }

    this.cssClass = this.setClass('counter');
  }

  ngOnChanges(changes: SimpleChanges): void {
    // if (changes.mods) {
    //   if (changes.mods.currentValue === State.Invalid) {
    //     this.isInvalid = true;
    //   } else {
    //     this.isInvalid = false;
    //   }
    // }
  }

  changeValue(count) {
    this.currentCount = count;
    this.onChange(count);
    this.change.emit(count);
  }

  writeValue(count) {
    this.currentCount = count || 0;
  }

  setCount(value) {
    this.currentCount = value;
    this.changeValue(this.currentCount);
  }

  countUp() {
    this.currentCount++;
    this.changeValue(this.currentCount);
  }

  countDown() {
    if (this.currentCount <= 0) {
      return;
    }

    this.currentCount--;
    this.changeValue(this.currentCount);
  }

  onFocus() {
    this.cdr.detectChanges();
    this.input.nativeElement.focus();
    this.input.nativeElement.select();
  }

  onBlur() {
    if (!this.currentCount) {
      this.currentCount = 0;
    }
  }

  go() {
    alert(1);
  }
}
